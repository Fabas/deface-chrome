
import { waterfall } from 'async'
import { Buffer } from 'buffer'
import libp2p from 'libp2p'
import WebRTCStar from 'libp2p-webrtc-star'
import Multiplex from 'libp2p-mplex'
import SECIO from 'libp2p-secio'
import defaultsDeep from '@nodeutils/defaults-deep'

import DefaceDHT from 'deface-dht'

import verificationManager from './VerificationManager.js'
import { getServerPublicKey } from './utils.js'
import { signallingServerHost, signallingServerPort } from '../../config.js'

/**
 * LibP2PNode connects to a swarm of peers in order to maintain
 * Deface's distributed hash table (deface-dht). It sets up listeners
 * for peer-to-peer connections, and regularly checks other peers'
 * verification status before sharing data with them.
 */
export default class LibP2PNode extends libp2p {
  /**
   * Create a LibP2PNode by initializing addresses, and overriding several DHT methods.
   *
   * @param {PeerInfo} peerInfo - Libp2p PeerInfo, contains peer id and addresses
   */
  constructor (peerInfo) {
    const peerIdStr = peerInfo.id.toB58String()
    const sigServerHostIsIP = signallingServerHost.split('.')
      .filter(block => block >= 0 && block < 256)
      .length === 4
    const ma = sigServerHostIsIP
      ? `/ip4/${signallingServerHost}/tcp/${signallingServerPort}/wss/p2p-webrtc-star/ipfs/${peerIdStr}`
      : `/dns4/${signallingServerHost}/tcp/${signallingServerPort}/wss/p2p-webrtc-star/ipfs/${peerIdStr}`
    peerInfo.multiaddrs.add(ma)

    const wrtcstar = new WebRTCStar({ id: peerInfo.id })

    const defaults = {
      peerInfo,
      modules: {
        transport: [wrtcstar],
        streamMuxer: [Multiplex],
        connEncryption: [SECIO],
        peerDiscovery: [wrtcstar.discovery],
        dht: DefaceDHT
      },
      config: {
        peerDiscovery: {
          webRTCStar: {
            enabled: true
          }
        },
        dht: {
          kBucketSize: 20
        },
        EXPERIMENTAL: {
          dht: true
        }
      }
    }

    super(defaultsDeep({}, defaults))

    this._dht.verifyPeer = this.verifyPeer.bind(this)
    this._dht.getVerification = verificationManager.getVerificationToken
    this._dht.getServerPublicKey = getServerPublicKey

    this.dht.get = (key, verification, callback) => {
      if (!this._dht) {
        return callback(new Error('DHT is not available'))
      }
      this._dht.get(key, verification, callback)
    }

    this.dht.getRequests = (peer, verificationDate, timeout, callback) => {
      if (!this._dht) {
        return callback(new Error('DHT is not available'))
      }
      this._dht.getRequests(peer, verificationDate, timeout, callback)
    }

    this.dht.addRequest = (verificationDate, key, signature, callback) => {
      if (!this._dht) {
        return callback(new Error('DHT is not available'))
      }
      this._dht.addRequest(verificationDate, key, signature, callback)
    }

    /**
     * Number of queries a node can make before it is required
     * to update its verification status.
     *
     * @type {number}
     */
    this.requestCountUntilVerification = 100

    this.setListeners = this.setListeners.bind(this)
    this.verifyPeer = this.verifyPeer.bind(this)

    verificationManager.setPeerInfo(this.peerInfo)
  }

  /**
   * Sets listeners for communications with other peers across the network.
   */
  setListeners () {
    this.on('peer:discovery', (peerInfo) => {
      this.dial(peerInfo, () => {})
    })

    this.on('peer:connect', (peer) => {
      console.log('peer connected: ', peer.id.toB58String())
    })

    this.on('peer:disconnect', (peer) => {
      console.log('peer disconnected: ', peer.id.toB58String())
    })
  }

  /**
   * Called from deface-dht. Verify a peer's verification status before sharing data with them:
   * 1. Check validity of their Deface verification token.
   * 2. Query DHT for all requests submitted with this verification token.
   * 3. Has this peer submitted a request record for the content currently queried?
   * 4. Check that peer has used this verification token less than [100] times.
   *
   * TODO: more robust error handling: pick where process should end
   *
   * @param {PeerId} peerId
   * @param {Buffer} key - Requested DHT key
   * @param {Buffer} verification - token containing a string id, a date, and a signature Buffer
   * @param {function(Error, boolean)} callback - does this peer need to update their verification status?
   */
  verifyPeer (peerId, key, verification, callback) {
    try {
      verification = JSON.parse(verification.toString())
    } catch (err) {
      return callback(err)
    }

    waterfall(
      [
        getServerPublicKey,
        (serverPublicKey, callback) => {
          const {
            id,
            date,
            signature
          } = verification

          const signedBuffer = Buffer.from(JSON.stringify({ id, date }))
          const signatureBuffer = Buffer.from(signature)
          serverPublicKey.verify(signedBuffer, signatureBuffer, callback)
        },
        (verificationIsValid, callback) => {
          if (!verificationIsValid) {
            return callback(new Error('verification is invalid'))
          }

          this.dht.getRequests(peerId, verification.date, 10000, callback)
        }
      ],
      (error, requests) => {
        if (error) {
          return callback(error)
        }

        const uniqueRequests = Object.keys(requests.reduce((map, request) => {
          map[request.key.toString()] = true
          return map
        }, {}))

        const keyString = key.toString().split('/')[2]
        if (!uniqueRequests.some(request => request === keyString)) {
          return callback(null, true)
        }

        const peerMustVerify = uniqueRequests.length > this.requestCountUntilVerification
        callback(null, peerMustVerify)
      }
    )
  }
}
