
import crypto from 'libp2p-crypto'

import { verificationServerUrl } from '../../config.js'

let serverPublicKey = null

/**
 * Get deface-server's public key. This is used for checking a
 * verification token's validity.
 *
 * @param {function(Error, RsaPublicKey)} callback
 */
export function getServerPublicKey (callback) {
  if (serverPublicKey) {
    return callback(null, serverPublicKey)
  }

  var xhr = new window.XMLHttpRequest()
  xhr.addEventListener('load', () => {
    let publicKey
    try {
      const buffer = Buffer.from(JSON.parse(xhr.responseText).data)
      publicKey = crypto.keys.unmarshalPublicKey(buffer)
      serverPublicKey = publicKey
    } catch (error) {
      console.log('Error getting server public key:', error)
      return callback(error)
    }
    return callback(null, publicKey)
  })
  xhr.open('GET', verificationServerUrl + '/public-key')
  xhr.send()
}
