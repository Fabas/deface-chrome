
import { series, waterfall, filter, filterSeries, each } from 'async'

import LocalKeychain from './LocalKeychain.js'
import DistributedKeychain from './DistributedKeychain.js'
import { encrypt, decrypt } from './crypto.js'
import verificationManager from './VerificationManager.js'

/**
 * Core module of the Deface app, KeychainManager processes 'put' and 'get'
 * requests from the client side. It manages both local and distributed
 * keychains, which store and propagate Deface keys across a distributed
 * network of peers.
 */
export default class KeychainManager {
  /**
   * Create a KeychainManager.
   *
   * @param {PeerInfo} peerInfo
   */
  constructor (peerInfo) {
    /**
     * A queue of 'get' requests submitted by the client side.
     *
     * @type {object}
     * @property {boolean} completed
     * @property {array.<object>} scripts - array of script objects
     * that contain a tabId and callback from the initial chrome message
     * @property {string} id - Deface message id
     * @property {string} cipherText - Deface message ciphertext
     */
    this.decryptionRequestsQueue = []

    /**
     * Are 'get' requests currently being processed?
     *
     * @type {boolean}
     */
    this.processingInProgress = false

    /**
     * Will there be more requests to process after the current batch?
     *
     * @type {boolean}
     */
    this.processingDeferred = true

    /**
     * The local keychain stores Deface message encryption keys in
     * Chrome's local storage.
     *
     * @type {LocalKeychain}
     */
    this.localKeychain = new LocalKeychain()

    /**
     * The distributed keychain stores Deface message encryption keys
     * in a distributed hash table (DHT) shared with all Deface users.
     *
     * @type {DistributedKeychain}
     */
    this.distributedKeychain = new DistributedKeychain(peerInfo)

    this.start = this.start.bind(this)
    this.putKey = this.putKey.bind(this)
    this.putLocalKeysIntoDistributedKeychain = this.putLocalKeysIntoDistributedKeychain.bind(this)
    this.getKey = this.getKey.bind(this)
    this.processRequests = this.processRequests.bind(this)
    this.processLocalRequest = this.processLocalRequest.bind(this)
    this.processDistributedRequest = this.processDistributedRequest.bind(this)
    this.getActiveContentScripts = this.getActiveContentScripts.bind(this)
  }

  /**
   * Initialize the distributed keychain network capabilities.
   *
   * @param {function(Error)} callback
   */
  start (callback) {
    this.distributedKeychain.start(callback)
  }

  /**
   * Encrypt a Deface message, and put the resulting encryption key into both
   * local and distributed keychains.
   *
   * @param {string} plainText
   * @param {function(object)} callback - pass a response object to the client side with these properties: error, cipherText, id.
   */
  putKey (plainText, callback) {
    const peerInfo = this.distributedKeychain.getPeerInfo()
    encrypt(peerInfo, plainText, (id, key, cipherText) => {
      series(
        [
          (callback) => { this.localKeychain.put(id, key, callback) },
          (callback) => { this.distributedKeychain.put(id, key, callback) }
        ],
        (error) => {
          if (error) {
            const res = {
              error,
              cipherText,
              id
            }

            return callback(res)
          }

          console.log('Successfully put key:', id, cipherText)

          const res = {
            error: null,
            cipherText,
            id
          }

          callback(res)
        }
      )
    })
  }

  /**
   * On startup, put all locally-stored keys into the distributed keychain.
   * This facilitates data persistence across the network.
   */
  putLocalKeysIntoDistributedKeychain () {
    this.localKeychain.getAll((entries) => {
      each(
        entries,
        (entry, callback) => {
          const {
            id,
            key
          } = entry

          this.distributedKeychain.put(id, key, callback)
        },
        (error) => {
          if (error) {
            return console.error('Error putting keys in DHT:', error)
          }
          console.log(`Successfully put ${Object.keys(entries).length} keys`)
        }
      )
    })
  }

  /**
   * Add a 'get' request to the queue. If a request for the same content
   * already exists, we just add another recipient to the existing request.
   *
   * @param {string} id - Deface message id
   * @param {string} cipherText - Deface message ciphertext
   * @param {number} tabId - Index of the Chrome tab where the query originated
   * @param {function(object)} callback - pass a response object to the client side with these properties: error, plainText.
   */
  getKey (id, cipherText, tabId, callback) {
    const request = this.decryptionRequestsQueue.find(request => request.id === id)

    if (!request) {
      const request = {
        completed: false,
        scripts: [
          {
            tabId,
            callback
          }
        ],
        id,
        cipherText
      }
      this.decryptionRequestsQueue.push(request)
    } else {
      request.scripts.push({
        tabId,
        callback
      })
    }

    this.processRequests()
  }

  /**
   * Empty the request queue and process each item it contained:
   * 1. We make sure the Chrome tabs that made the request are still active.
   * 2. We query the local keychain for the requested Deface id.
   * 3. If the local keychain did not have any entry for the given id, we query the distributed keychain.
   * 4. Any unresolved request is sent back to the queue.
   * 5. If the distributed keychain returned a verification error, the process is interrupted and the user will be required to verify themselves again.
   *
   * TODO: there should be no request remaining after this.processDistributedRequest was called.
   *
   * If this method is called  while the processing is already ongoing,
   * the new requests are deferred until the current batch is processed.
   */
  processRequests () {
    if (this.processingInProgress) {
      this.processingDeferred = true
      return null
    }

    this.processingInProgress = true
    const requests = this.decryptionRequestsQueue.slice(0)
    this.decryptionRequestsQueue = []

    waterfall(
      [
        (callback) => {
          filter(
            requests,
            (request, callback) => this.getActiveContentScripts(
              request,
              (error, scripts) => {
                callback(error, scripts.length > 0)
              }
            ),
            callback
          )
        },
        (activeRequests, callback) => {
          filter(
            activeRequests,
            this.processLocalRequest,
            (error, remainingRequests) => {
              if (error) {
                const untreatedRequests = activeRequests.filter(request => !request.completed)
                return callback(error, untreatedRequests)
              }
              callback(null, remainingRequests)
            }
          )
        },
        (requests, callback) => {
          filterSeries(
            requests,
            this.processDistributedRequest,
            (error, remainingRequests) => {
              if (error) {
                const untreatedRequests = requests.filter(request => !request.completed)
                return callback(error, untreatedRequests)
              }

              if (remainingRequests.length > 0) {
                console.error('There are remaining requests at the end of processing')
              }

              callback(null, remainingRequests)
            }
          )
        }
      ],
      (error, requests) => {
        this.decryptionRequestsQueue = requests.concat(this.decryptionRequestsQueue)
        this.processingInProgress = false

        if (error) {
          console.error(error)

          if (error === 'verification error') {
            console.log('Verification required, halting request processing')
            this.processingDeferred = false
            verificationManager.requireVerification(true)
          }
        }

        if (this.processingDeferred) {
          this.processingDeferred = false
          this.processRequests()
        }
      }
    )
  }

  /**
   * Handle a request locally by submitting it to the local keychain.
   * If the local keychain returns an encryption key for the provided Deface id,
   * it is used to decrypt the ciphertext provided. A response object is then
   * sent to each Chrome tab attached to the request.
   * Unresolved requests are passed along to be processed by the distributed keychain.
   *
   * @param {object} request
   * @param {string} request.id - Deface message id
   * @param {string} request.cipherText - Deface message id
   * @param {array.<object>} request.scripts - contains a tabId number and a callback to be summoned when the request is fulfilled.
   * @param {function(Error, boolean)} callback - does this request need to be sent to the distributed keychain?
   */
  processLocalRequest (request, callback) {
    const {
      id,
      cipherText
    } = request

    waterfall(
      [
        (callback) => this.localKeychain.get(id, cipherText, callback),
        (id, key, cipherText, callback) => decrypt(id, key, cipherText, callback)
      ],
      (error, id, key, plainText) => {
        if (error || !plainText) {
          // Unable to find key in local keychain, will query distributed keychain
          if (error && error.message !== 'no key') {
            console.error(error)
          }

          return callback(null, true)
        }

        this.getActiveContentScripts(request, (error, activeScripts) => {
          if (error) {
            console.error(error)
          }

          activeScripts.forEach((script, i) => {
            script.callback({ plainText })
          })
        })

        request.completed = true
        callback(null, false)
      }
    )
  }

  /**
   * Handle a request by submitting it to the distributed keychain.
   * If the distributed keychain returns an encryption key for the provided Deface id,
   * it is used to decrypt the request's ciphertext, and stored into the local keychain.
   * A response object is then sent to each Chrome tab attached to the request.
   *
   * If the distributed keychain returns an error (e.g verification request), the request
   * will be sent back to the queue. If no error was returned but a key could not be found,
   * an empty response is sent to the Chrome tabs attached to the request.
   *
   * @param {object} request
   * @param {string} request.id - Deface message id
   * @param {string} request.cipherText - Deface message id
   * @param {array.<object>} request.scripts - contains a tabId number and a callback to be summoned when the request is fulfilled.
   * @param {function(Error)} callback
   */
  processDistributedRequest (request, callback) {
    const {
      id,
      cipherText
    } = request

    waterfall(
      [
        (callback) => this.distributedKeychain.get(id, cipherText, callback),
        (id, key, cipherText, callback) => decrypt(id, key, cipherText, callback),
        (id, key, plainText, callback) => this.localKeychain.put(id, key, (error) => callback(error, plainText))
      ],
      (error, plainText) => {
        if (error) {
          if (error === 'verification error') {
            return callback(error)
          }

          this.getActiveContentScripts(request, (error, activeScripts) => {
            activeScripts.forEach(script => {
              script.callback({ error })
            })
          })
          request.completed = true
          return callback(null)
        }

        if (!plainText) {
          this.getActiveContentScripts(request, (error, activeScripts) => {
            if (!error) {
              activeScripts.forEach(script => {
                script.callback({})
              })
            }
          })
          request.completed = true
          return callback(null)
        }

        this.getActiveContentScripts(request, (error, activeScripts) => {
          if (!error) {
            activeScripts.forEach(script => {
              script.callback({ plainText })
            })
          }
        })

        request.completed = true
        callback(null)
      }
    )
  }

  /**
   * Get all active Chrome tabs attached to a request.
   *
   * @param {object} request
   * @param {string} request.id - Deface message id
   * @param {string} request.cipherText - Deface message id
   * @param {array.<object>} request.scripts - contains a tabId number and a callback to be summoned when the request is fulfilled.
   * @param {function(Error, array.<object>)} callback - passes an array of active script objects.
   */
  getActiveContentScripts (request, callback) {
    filter(
      request.scripts,
      (script, callback) => {
        try {
          window.chrome.tabs.get(script.tabId, tab => {
            if (!tab) {
              return callback(null, false)
            }
            return callback(null, true)
          })
        } catch (error) {
          return callback(null, false)
        }
      },
      callback
    )
  }
}
