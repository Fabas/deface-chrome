
/**
 * LocalKeychain puts and gets Deface encryption keys stored
 * in Chrome's local storage. For each Deface id corresponds a key object.
 */
export default class LocalKeychain {
  /**
   * Create a LocalKeychain.
   */
  constructor () {
    this.put = this.put.bind(this)
    this.get = this.get.bind(this)
    this.getAll = this.getAll.bind(this)
  }

  /**
   * Put an encryption key object into Chrome's local storage.
   *
   * @param {string} id - Deface message id
   * @param {object} key
   * @param {string} key.key - encryption key
   * @param {string} key.iv - initialization vector
   * @param {function} callback
   */
  put (id, key, callback) {
    const storeValue = {
      [id]: key
    }
    window.chrome.storage.sync.set(storeValue, callback.bind(this))
  }

  /**
   * Get a Deface encryption key from Chrome's local storage.
   *
   * @param {string} id - Deface message id
   * @param {string} cipherText
   * @param {function(Error, string, object, string)} callback - upon success, contains a message id, an encryption key object, and the initial ciphertext
   */
  get (id, cipherText, callback) {
    console.log('getting key from local store:', id, cipherText)

    window.chrome.storage.sync.get(id, storeValue => {
      const key = storeValue[id]

      if (!key) {
        return callback(new Error('no key'))
      }

      if (!key.hasOwnProperty('key') || !key.hasOwnProperty('iv')) {
        return callback(new Error('invalid key'))
      }

      console.log('Received decryption key from local store:', id, key)
      callback(null, id, key, cipherText)
    })
  }

  /**
   * Get all Deface encryption keys stored in Chrome's local storage.
   *
   * @param {function(array.<object>)} callback - passes an array of encryption keys wrapped with their Deface id
   */
  getAll (callback) {
    window.chrome.storage.sync.get(null, data => {
      const entries = Object.keys(data)
        .map(id => {
          return {
            id,
            key: data[id]
          }
        })
      callback(entries)
    })
  }
}
