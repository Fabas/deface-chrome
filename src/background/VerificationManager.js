
import { Buffer } from 'buffer'
import { waterfall } from 'async'

import { getServerPublicKey } from './utils.js'
import { verificationServerUrl } from '../../config.js'

import iconUrl from '../_img/icon-128.png'

/**
 * This class manages user verification. Specifically, it handles
 * reCaptcha verification tokens, communicates with deface-server
 * to validate verification, and manages Deface verification tokens used
 * as part of the distributed keychain.
 */
class VerificationManager {
  /**
   * Create a VerificationManager.
   */
  constructor () {
    /**
     * Indicates whether the user needs to verify before accessing more data.
     *
     * @type {boolean}
     */
    this.captchaRequired = false

    /**
     * Stores Deface verification tokens delivered by deface-server.
     *
     * @type {object}
     * @property {number} date
     * @property {string} id
     * @property {Buffer} signature
     */
    this.verificationToken = null

    /**
     * Store PeerInfo describing this node. Contains user id and addresses.
     *
     * @type {PeerInfo}
     */
    this.peerInfo = null

    this.setPeerInfo = this.setPeerInfo.bind(this)
    this.setVerificationToken = this.setVerificationToken.bind(this)
    this.getVerificationToken = this.getVerificationToken.bind(this)
    this.requireVerification = this.requireVerification.bind(this)
    this.verifySelf = this.verifySelf.bind(this)
  }

  /**
   * Set PeerInfo for this libp2p node.
   *
   * @param {PeerInfo} peerInfo
   */
  setPeerInfo (peerInfo) {
    this.peerInfo = peerInfo
  }

  /**
   * Set Deface verification token for this users. These tokens are received
   * from deface-server.
   *
   * @param {number} date
   * @param {string} id
   * @param {Buffer} signature
   */
  setVerificationToken (date, id, signature) {
    this.verificationToken = {
      date,
      id,
      signature
    }
  }

  /**
   * Get Deface verification token. Used by deface-dht on ping.
   *
   * @return {object}
   */
  getVerificationToken () {
    return this.verificationToken
  }

  /**
   * Set the verification status. If required, trigger a Chrome
   * notification prompting the user to solve a reCaptcha puzzle.
   *
   * @param {boolean} required
   */
  requireVerification (required) {
    this.captchaRequired = required
    if (required) {
      window.chrome.notifications.create({
        type: 'basic',
        iconUrl: iconUrl,
        title: 'Deface',
        message: 'You need to verify yourself in order to decrypt new Deface messages.'
      })
    }
  }

  /**
   * Send a reCaptcha token from verification/index.html over to
   * deface-server, which responds with its own Deface verification token.
   * The token's signature is verified with the latest deface-server public key,
   * and the user's verification status is updated accordingly.
   *
   * @param {string} token - Google reCaptcha token
   * @param {function(Error, boolean)} callback - was the user successfully verified?
   */
  verifySelf (token, callback) {
    if (!this.peerInfo) {
      return callback(new Error('No peerInfo provided'))
    }

    const id = this.peerInfo.id.toB58String()
    const xhr = new window.XMLHttpRequest()
    const params = `token=${token}&id=${id}`

    xhr.open('POST', verificationServerUrl + '/verify', true)
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded')
    xhr.addEventListener('load', () => {
      try {
        const response = JSON.parse(xhr.responseText)

        if (response.error) {
          return callback(response.error)
        }

        const {
          date,
          id,
          signature
        } = response

        waterfall([
          (callback) => getServerPublicKey(callback),
          (serverPublicKey, callback) => {
            const signedBuffer = Buffer.from(JSON.stringify({ id, date }))
            const signatureBuffer = Buffer.from(signature.data)
            serverPublicKey.verify(signedBuffer, signatureBuffer, callback)
          }
        ], (error, verified) => {
          if (error) {
            return callback(error)
          }

          this.requireVerification(false)
          this.setVerificationToken(date, id, signature)
          callback(null, true)
        })
      } catch (error) {
        return callback(error)
      }
    })
    xhr.send(params)
  }
}

export default new VerificationManager()
