
import { Buffer } from 'buffer'
import bs58 from 'bs58'
import forge from 'node-forge'
import multihashes from 'multihashes'

/**
 * Encrypt plaintext and generate a Deface id for a given message.
 *
 * @param {PeerInfo} peerInfo
 * @param {string} plainText
 * @param {function(string, object, string)} callback - passes a Deface message id, an encryption key object, and ciphertext
 */
export function encrypt (peerInfo, plainText, callback) {
  const peerId = peerInfo.id.toB58String()
  const key = forge.random.getBytesSync(32)
  const iv = forge.random.getBytesSync(16)

  const cipher = forge.cipher.createCipher('AES-CBC', key)
  cipher.start({ iv: iv })
  cipher.update(forge.util.createBuffer(plainText))
  cipher.finish()
  const cipherText = cipher.output.toHex()

  const buf = Buffer.from(peerId.slice(-6) + '-' + Date.now())
  const mh = multihashes.encode(buf, 'sha1')
  const id = bs58.encode(mh)

  callback(id, { key, iv }, cipherText)
}

/**
 * Decrypt a Deface message given ciphertext and an encryption key object.
 *
 * @param {string} id - Deface message id
 * @param {object} key
 * @param {string} key.key - encryption key
 * @param {string} key.iv - initialization vector
 * @param {string} cipherText
 * @param {function(Error, string, object, string)} callback
 */
export function decrypt (id, key, cipherText, callback) {
  const decipher = forge.cipher.createDecipher('AES-CBC', key.key)
  decipher.start({ iv: key.iv })
  const cipherBytes = forge.util.hexToBytes(cipherText)
  const cipherBuffer = forge.util.createBuffer(cipherBytes)
  decipher.update(cipherBuffer)
  const result = decipher.finish()

  if (!result) {
    return callback(new Error(`No result from decryption: ${id} ${key.key} ${cipherText}`))
  }

  let plainText = ''

  try {
    plainText = decipher.output.toString()
  } catch (error) {
    return callback(new Error(`Error while reading output: ${error}`))
  }

  callback(null, id, key, plainText)
}
