'use strict'

import PeerInfo from 'peer-info'

import KeychainManager from './KeychainManager.js'
import verificationManager from './VerificationManager.js'
import { recaptchaSiteKey } from '../../config.js'

import '../_img/icon-16.png'
import '../_img/icon-19.png'
import '../_img/icon-38.png'
import '../_img/icon-48.png'
import '../_img/icon-128.png'

if (!recaptchaSiteKey) {
  throw new Error('You need to set your reCaptcha site key in config.js before using deface-chrome. Documentation available at https://gitlab.com/deface/deface')
}

/**
 * This class manages all background tasks for Deface, including reCaptcha
 * verification, encrypting/decrypting messages, and getting/putting Deface
 * keys in the local and distributed key stores.
 */
class Deface {
  /*
   * Generate this node's PeerInfo, instantiate KeychainManager, initialize message
   * handler, and prompt the user to start the verification process by
   * opening verification.html in a new tab.
   */
  constructor () {
    this.status = 'off'

    this.start = this.start.bind(this)
    this.onMessage = this.onMessage.bind(this)
    this.verifySelf = this.verifySelf.bind(this)

    PeerInfo.create((error, peerInfo) => {
      if (error) {
        return console.error(error)
      }

      this.keychainManager = new KeychainManager(peerInfo)
      window.chrome.runtime.onMessage.addListener(this.onMessage)
      window.chrome.tabs.create({ url: window.chrome.extension.getURL('verification.html') })
    })
  }

  /**
   * Upon first reCaptcha verification, initialize this.keychainManager
   * and put local Deface keys into the distributed keychain.
   */
  start () {
    this.status = 'starting'
    this.keychainManager.start((error) => {
      if (error) {
        // TODO: handle error
        return console.error(error)
      }

      this.keychainManager.putLocalKeysIntoDistributedKeychain()
      this.status = 'on'
    })
  }

  /**
   * Handle messages received from content scripts and the verification page.
   * - 'isReady' checks if Deface's background is ready (always true if it makes it this far).
   * - 'put' requires encrypting a new message and putting the resulting encryption keys into both local and distributed keychains.
   * - 'get' requires the plaintext for a given encrypted message.
   * - 'verify' sends a reCaptcha token to VerificationManager for validation.
   *
   * @param {Object} request
   * @param {string} request.type - Type of message.
   * @param {string} [request.plainText] - Content to encrypt.
   * @param {string} [request.token] - Google reCaptcha frontend token
   * @param {Object} sender - Chrome tab that sent the message.
   * @param {function(Object)} callback - Pass a response object back to sender.
   * @return {boolean}
   */
  onMessage (request, sender, callback) {
    if (this.status === 'on' && !!sender.tab && sender.tab.url.indexOf('facebook.com') > -1) {
      switch (request.type) {
        case 'isReady' :
          const res = { ready: true }
          callback(res)
          break
        case 'put' :
          this.keychainManager.putKey(request.plainText, callback)
          break
        case 'get' :
          const {
            id,
            cipherText
          } = request
          this.keychainManager.getKey(id, cipherText, sender.tab.id, callback)
          break
        default :
          const emptyRes = {}
          callback(emptyRes)
      }
    } else {
      if (request.type === 'verify') {
        this.verifySelf(request.token, callback)
      }
    }
    return true
  }

  /**
   * Prompted by a 'verify' message, this calls verificationManager
   * to validate the user's reCaptcha token. The first successful
   * verification will trigger the keychainManager initialization.
   *
   * @param {string} token - reCaptcha frontend token.
   * @param {function(Error, boolean)} callback
   */
  verifySelf (token, callback) {
    verificationManager.verifySelf(token, (error, verified) => {
      if (error) {
        console.error(error)
        return callback(error, null)
      }

      if (!verified) {
        console.error('Failed verifying self')
        return callback(null, false)
      }

      if (this.status === 'off') {
        this.start()
      }

      if (this.status === 'on') {
        this.keychainManager.processRequests()
      }

      callback(null, true)
    })
  }
}

const deface = new Deface()
