
import { waterfall } from 'async'

import LibP2PNode from './LibP2PNode.js'
import verificationManager from './VerificationManager.js'

/**
 * DistributedKeychain puts and gets Deface encryption keys stored
 * in Deface's distributed hash table (DHT). For each Deface id corresponds a key object.
 * To access deface-dht, DistributedKeychain relies on LibP2PNode, a module
 * which connects to the swarm of peers that maintains the DHT.
 */
export default class DistributedKeychain {
  /**
   * Create a DistributedKeychain.
   *
   * @param {PeerInfo} peerInfo
   */
  constructor (peerInfo) {
    /**
     * LibP2P node, handles network capabilities.
     *
     * @type {LibP2PNode}
     */
    this.libP2PNode = new LibP2PNode(peerInfo)

    this.start = this.start.bind(this)
    this.put = this.put.bind(this)
    this.get = this.get.bind(this)
    this.putRequest = this.putRequest.bind(this)
    this.getPeerInfo = this.getPeerInfo.bind(this)
  }

  /**
   * Initialize Libp2p node.
   *
   * @param {function} callback
   */
  start (callback) {
    this.libP2PNode.start((error) => {
      if (error) {
        return callback(new Error('Could not initialize node'))
      }

      console.log('Started libp2p node', this.libP2PNode.peerInfo.id.toB58String())
      this.libP2PNode.setListeners()
      callback()
    })
  }

  /**
   * Put an encryption key object into Deface's distributed hash table.
   *
   * @param {string} id - Deface message id
   * @param {object} key
   * @param {string} key.key - encryption key
   * @param {string} key.iv - initialization vector
   * @param {function(Error)} callback
   */
  put (id, key, callback) {
    const idBuffer = Buffer.from(`/value/${id}`)
    const keyBuffer = Buffer.from(JSON.stringify(key))

    this.libP2PNode.dht.put(idBuffer, keyBuffer, error => {
      // if (error && Object.keys(error).length > 0) {
      if (error) {
        return callback(error)
      }

      callback(null)
    })
  }

  /**
   * Get a Deface encryption key from Deface's distributed hash table. A node is
   * required to submit a request record to the network before it can query a
   * Deface encryption key. This enables the network to monitor user activity.
   *
   * @param {string} id
   * @param {string} cipherText
   * @param {function(Error, string, response, cipherText)} callback - upon success, contains a message id, an encryption key object, and the initial ciphertext
   */
  get (id, cipherText, callback) {
    console.log('getting key from distributed store', id)

    const verificationToken = verificationManager.getVerificationToken()
    if (!verificationToken || !verificationToken.signature) {
      return callback(new Error('Verification required'), null)
    }

    const idBuffer = Buffer.from(`/value/${id}`)
    const verificationBuffer = Buffer.from(JSON.stringify(verificationToken))

    waterfall(
      [
        (callback) => {
          this.putRequest(verificationToken, id, callback)
        },
        (callback) => {
          this.libP2PNode.dht.get(idBuffer, verificationBuffer, callback)
        }
      ],
      (error, data) => {
        if (error) {
          return callback(error)
        }

        try {
          const response = JSON.parse(data.toString())

          if (response.error) {
            return callback(response.error)
          }

          // TODO - check response
          if (!response.key || !response.iv) {
            return callback(new Error('Invalid response from DHT'))
          }

          console.log('Received decryption key from distributed store:', id, response)
          callback(null, id, response, cipherText)
        } catch (error) {
          callback(error)
        }
      }
    )
  }

  /**
   * A node is required to submit a request record to the network before it can query a
   * Deface encryption key. This request record is signed and pushed to the distributed
   * hash table.
   *
   * Note: the request object uses deface-dht terminology, where `key` refers to a
   * Deface message id. We keep this terminology in deface-dht in order to be consistent
   * with js-libp2p-kad-dht, but use the attribute `id` in deface-chrome where the word
   * `key` is used to mean something else.
   *
   * @param {object} verification - Deface verification token
   * @param {string} verification.id - peer id
   * @param {number} verification.date
   * @param {Buffer} verification.signature
   * @param {string} id - Deface message id
   * @param {function(Error)} callback
   */
  putRequest (verification, id, callback) {
    const signedBuffer = Buffer.from(
      JSON.stringify({
        peerId: verification.id,
        date: verification.date,
        key: id
      })
    )

    this.libP2PNode.peerInfo.id.privKey.sign(signedBuffer, (error, signature) => {
      if (error) {
        return callback(error)
      }

      this.libP2PNode.dht.addRequest(verification.date, id, signature, error => {
        if (error && Object.keys(error).length > 0) {
          return callback(error)
        }

        callback(null)
      })
    })
  }

  /**
   * Get this node's peer info.
   *
   * @return {PeerInfo}
   */
  getPeerInfo () {
    return this.libP2PNode.peerInfo
  }
}
