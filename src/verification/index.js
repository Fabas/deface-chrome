
import { recaptchaSiteKey } from '../../config.js'

document.addEventListener('DOMContentLoaded', event => {
  const recaptchaElement = document.createElement('div')
  recaptchaElement.className = 'g-recaptcha'
  recaptchaElement.setAttribute('data-sitekey', recaptchaSiteKey)
  recaptchaElement.setAttribute('data-callback', 'verifyRecaptcha')

  document.querySelector('.recaptcha').appendChild(recaptchaElement)

  window.verifyRecaptcha = function (token) {
    window.chrome.runtime.sendMessage(
      {
        type: 'verify',
        token
      },
      (error, response) => {
        // TODO: handle error
        if (error) {
          console.error(error)
        }

        window.setTimeout(() => {
          window.close()
        }, 1000)
      }
    )
  }
})
