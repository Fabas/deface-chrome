import loadingIcon from '../_img/loading.gif'
import successIcon from '../_img/checkMark.svg'
import errorIcon from '../_img/notfound.svg'

/**
 * DecryptionSandbox receives a Deface id and a ciphertext string,
 * which it communicates to the extensions' background script. Upon
 * successful decryption, it renders a sandboxed iframe that displays
 * the resulting plaintext.
 */
export default class DecryptionSandbox {
  /**
   * Create a DecryptionSandbox.
   *
   * @param {Object} element - the container div where plaintext will be rendered.
   * @param {string} id - the encrypted post's Deface id.
   * @param {string} cipherText - the encrypted post's ciphertext.
   */
  constructor (element, id, cipherText) {
    /**
     * DOM element where the resulting plaintext will be rendered.
     *
     * @type {Object}
     */
    this.element = element

    /**
     * The encrypted post's Deface id.
     *
     * @type {string}
     */
    this.id = id

    /**
     * The initial post's content, encrypted.
     *
     * @type {string}
     */
    this.cipherText = cipherText

    /**
     * The plaintext content rendered upon decryption.
     *
     * @type {string}
     */
    this.plainText = null

    /**
     * Text content as it is currently rendered.
     *
     * @type {string}
     */
    this.currentText = `${this.id}\n${this.cipherText}\nDeface is looking for decryption keys...`

    /**
     * Interpolation factor used during the transition between this.cipherText and this.plainText.
     *
     * @type {number}
     */
    this.alpha = 0

    /**
     * Determines how fast this.alpha will increment toward 1.
     *
     * @type {number}
     */
    this.speed = 0.2

    /**
     * Sandboxed iframe containing the animated text.
     *
     * @type {Object}
     */
    this.sandbox = null

    this.init = this.init.bind(this)
    this.setText = this.setText.bind(this)
    this.getPlainText = this.getPlainText.bind(this)
    this.animate = this.animate.bind(this)
    this.onError = this.onError.bind(this)
    this.setIcon = this.setIcon.bind(this)
  }

  /**
   * Initialize and append the sandboxed iframe element to the DOM.
   * Call this.getPlainText().
   *
   * @return {DecryptionSandbox} this
   */
  init () {
    const sandboxContent = `
      <!DOCTYPE html>
      <html style="height: 100%">
        <head>
          <meta charset="utf-8">
          <meta http-equiv="Content-Security-Policy" content="default-src 'self'; style-src 'self' 'unsafe-inline'; img-src 'self' data:;">
          <style>
            body {
              font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
              font-size: 14px;
              line-height: 20px;
            }
          </style>
        </head>
        <body style="overflow: hidden; margin: 0; height: 100%">
          <div id="content" style="height: auto; width: 100%;">
            <span>
              ${this.currentText}
            </span>
            <img
              width="16px"
              height="16px"
              src="${loadingIcon}"
              style="
                vertical-align: text-bottom;
                transition: opacity 0.2s, transform 0.35s;
                margin-left: 5px;
              "
            />
          </div>
        </body>
      </html>
    `

    this.sandbox = document.createElement('iframe')
    this.sandbox.style.border = 'none'
    this.sandbox.style.width = '100%'
    this.sandbox.style.height = '100%'
    this.sandbox.setAttribute('srcDoc', sandboxContent)
    this.sandbox.setAttribute('sandbox', 'allow-same-origin')
    this.sandbox.setAttribute('frameBorder', 0)
    this.element.appendChild(this.sandbox)

    this.getPlainText()

    return this
  }

  /**
   * Send a message to Deface's background script requiring decryption of
   * the provided ciphertext. Handle the response by displaying an error
   * or starting the animated transition from ciphertext to plaintext.
   */
  getPlainText () {
    window.chrome.runtime.sendMessage(
      {
        type: 'get',
        id: this.id,
        cipherText: this.cipherText
      },
      (response) => {
        if (!response) {
          return console.error('empty response')
        }

        const {
          plainText,
          error
        } = response

        if (error || !plainText) {
          console.error(error || 'Empty plain text')
          return this.onError()
        }

        this.plainText = plainText
        this.animate()
      }
    )
  }

  /**
   * Set the text to display in the iframe.
   *
   * @param {string} text
   */
  setText (text) {
    this.currentText = text
    const textElement = this.sandbox.contentWindow.document.querySelector('#content span')
    textElement.innerText = this.currentText
  }

  /**
   * Set the icon that follows the text displayed in the iframe,
   * and initiate its animation.
   *
   * @param {string} type
   */
  setIcon (type) {
    const iconElement = this.sandbox.contentWindow.document.querySelector('#content img')
    const src = type === 'success' ? successIcon : errorIcon
    iconElement.setAttribute('src', src)
    iconElement.style.opacity = 0
    iconElement.style.transform = 'scale(0.1) rotate(90deg)'

    window.requestAnimationFrame(() => {
      iconElement.style.opacity = 1
      iconElement.style.transform = 'scale(1) rotate(0)'
    })
  }

  /**
   * Animate the iframe-contained text, as it transitions from ciphertext to plaintext.
   */
  animate () {
    const text = Array.prototype.slice.call(this.currentText)
      .map((c, i) => {
        if (this.plainText.charAt(i) === c || Math.random() < this.alpha) {
          return this.plainText.charAt(i)
        }
        return Math.random().toString(36).slice(2)[0]
      })
      .join('')

    this.alpha = (1 - this.alpha) * this.speed

    this.setText(text)

    if (text === this.plainText) {
      this.setIcon('success')
    } else {
      window.requestAnimationFrame(() => {
        this.animate()
      })
    }
  }

  /**
   * Display an error message.
   */
  onError () {
    this.setText(`${this.id}\n${this.cipherText}\nDeface could not decrypt this message`)
    this.setIcon('error')
  }
}
