
import DecryptionSandbox from './DecryptionSandbox'

const element = document.createElement('div')
document.body.appendChild(element)

const query = new URLSearchParams(document.location.search)
const id = query.get('id')
const cipherText = query.get('ciphertext')

new DecryptionSandbox(element, id, cipherText).init()
