
import DecryptionFrame from './DecryptionFrame'

/**
 * This class manages interactions between Deface and the Facebook
 * page after it was injected with this script.
 *
 * It tracks changes to the Facebook feed DOM to capture Deface
 * posts and inject DecryptionFrame objects that display resulting plaintext.
 *
 * It also captures new messages submitted by the user to encrypt them
 * before sending them to Facebook.
 */
export default class ContentScript {
  /**
   * Create a ContentScript.
   */
  constructor () {
    /**
     * DOM element for the Facebook post composer.
     *
     * @type {Object}
     */
    this.composerElement = null

    /**
     * DOM element for the Facebook feed.
     *
     * @type {Object}
     */
    this.feedElement = null

    /**
     * DOM element for the submit button, part of the Facebook post composer.
     *
     * @type {Object}
     */
    this.submitElement = null

    /**
     * MutationObserver that keeps track of changes in the #content div of the Facebook page.
     *
     * @type {MutationObserver}
     */
    this.contentMutationObserver = null

    /**
     * MutationObserver that keeps track of changes in the composer element of the Facebook page.
     *
     * @type {MutationObserver}
     */
    this.composerMutationObserver = null

    /**
     * MutationObserver that keeps track of changes in the feed element of the Facebook page.
     *
     * @type {MutationObserver}
     */
    this.feedMutationObserver = null

    /**
     * Facebook post DOM elements captured in the feed.
     *
     * @type {Array.<Object>}
     */
    this.postElements = []

    /**
     * A list of all DecryptionFrame objects injected in the feed to replace encrypted posts.
     *
     * @type {Array.<DecryptionFrame>}
     */
    this.decryptionFrames = []

    this.init = this.init.bind(this)
    this.onPageInitialized = this.onPageInitialized.bind(this)
    this.onComposerMutated = this.onComposerMutated.bind(this)
    this.onFeedMutated = this.onFeedMutated.bind(this)
    this.onPageChange = this.onPageChange.bind(this)
    this.post = this.post.bind(this)
  }

  /**
   * Initialize by identifying the #content div in the page
   * and attaching a MutationObserver object to it that will trigger when new
   * content is loaded, or when the page changes.
   *
   * @return {ContentScript} this
   */
  init () {
    const contentElement = document.querySelector('#content')

    this.contentMutationObserver = new window.MutationObserver(this.onPageInitialized)
    this.contentMutationObserver.observe(contentElement, { childList: true, subtree: true })

    const pageChangeObserver = new window.MutationObserver(this.onPageChange)
    pageChangeObserver.observe(contentElement, { childList: true, subtree: false })

    return this
  }

  /**
   * On page start and page change, start keeping track of the composer and feed
   * DOM elements. Attach MutationObserver objects to both, which will trigger as
   * the composer is activated or the Facebook timeline is updated. We stop
   * monitoring changes to the #content div after this.
   *
   * TODO: the DOM selection here is very hacky and needs to be replaced with
   * something robust.
   */
  onPageInitialized () {
    if (!this.composerElement) {
      this.composerElement =
        document.getElementById('pagelet_composer') ||
        document.getElementById('timeline_composer_container')

      if (this.composerElement) {
        this.composerMutationObserver = new window.MutationObserver(this.onComposerMutated)
        this.composerMutationObserver.observe(this.composerElement, { childList: true, subtree: true })
      }
    }

    if (!this.feedElement) {
      this.feedElement =
        document.querySelector('div[aria-label="News Feed"]') ||
        document.querySelector('#timeline_story_column') ||
        document.querySelector('#recent_capsule_container')

      if (this.feedElement) {
        this.feedMutationObserver = new window.MutationObserver(this.onFeedMutated)
        this.feedMutationObserver.observe(this.feedElement, { childList: true, subtree: true })
        this.onFeedMutated()
      }
    }

    if (this.feedElement && this.composerElement) {
      this.contentMutationObserver.disconnect()
    }
  }

  /**
   * On feed update, parse new content to find Deface posts and add them to
   * this.postElements array. Each new post is assigned a new DecryptionFrame object.
   *
   * TODO:  robust pattern matching and dom selection
   * TODO:  need content sanitization
   */
  onFeedMutated () {
    const regexp = /\"?[a-zA-Z0-9]{30}\"?<br>\s*\"?\s*[a-z0-9]+\"?/g

    const newPosts = Array.prototype.slice.call(this.feedElement.querySelectorAll('._5pbx, ._5_jv'))
      .filter(element => this.postElements.indexOf(element) === -1)

    newPosts.forEach(element => {
      const contentHTML = element.innerHTML
      const match = contentHTML.match(regexp)

      if (match && match.length === 1) {
        let decryptionFrame = this.decryptionFrames.find(p => p.element === element)
        if (!decryptionFrame) {
          const content = match[0]
          decryptionFrame = new DecryptionFrame().init(element, content)
          this.decryptionFrames.push(decryptionFrame)
        }
      }

      this.postElements.push(element)
    })
  }

  /**
   * When the composer is activated, we start keeping track of the submit
   * button and create a clone of it, that's dedicated to posting Deface encrypted messages.
   */
  onComposerMutated () {
    const submitElement = this.composerElement.querySelector('button[type="submit"].selected:not(.deface)')

    if (this.submitElement !== submitElement) {
      this.submitElement = submitElement

      if (this.submitElement) {
        const defaceButtonElement = this.submitElement.cloneNode(false)

        defaceButtonElement.className += ' deface'
        defaceButtonElement.innerHTML = 'Share with Deface'
        defaceButtonElement.removeAttribute('disabled')
        defaceButtonElement.addEventListener('click', this.post)
        defaceButtonElement.setAttribute('style', `
          border-color: #db3a4d;
          background-color: #db3a4d;
          color: white;
          cursor:pointer;
          display: block;
          position: relative;
        `)

        this.submitElement.parentNode.setAttribute('style', 'display: flex;')
        this.submitElement.parentNode.appendChild(defaceButtonElement)
      }
    }
  }

  /**
   * Handle page change by disconnecting all MutationObserver objects and restarting
   * the initialization process.
   */
  onPageChange () {
    if (this.contentMutationObserver) {
      this.contentMutationObserver.disconnect()
    }

    if (this.composerMutationObserver) {
      this.composerMutationObserver.disconnect()
    }

    if (this.feedMutationObserver) {
      this.feedMutationObserver.disconnect()
    }

    this.init()
  }

  /**
   * Triggers when the user clicks this.defaceButtonElement. This captures
   * the plaintext entered in the composer's input field element, sends that
   * content to Deface's background script, and then swaps the initial
   * plaintext with encrypted content before sending it to Facebook.
   *
   * TODO: this causes a security vulnerability since Facebook could simply
   * record plaintext as it is entered in the input field. Deface messages
   * need to be typed in a sandboxed context.
   *
   * TODO: the DOM selection here is very hacky and need to be replaced with
   * something robust.
   */
  post () {
    const inputElement = document.querySelector('#pagelet_composer ._1mf span span, #timeline_composer_container ._1mf span span')
    inputElement.click()

    window.chrome.runtime.sendMessage(
      {
        type: 'put',
        plainText: inputElement.innerText
      },
      (response) => {
        if (!response) {
          return console.error('Empty response after posting.')
        }

        const {
          error,
          cipherText,
          id
        } = response

        if (error) {
          return console.error('Error while posting:', error)
        }

        const postContent = `${id}\n${cipherText}\nThis message was encrypted with Deface: http://ian.earth/projects/deface/`
        inputElement.innerText = postContent
        inputElement.value = postContent

        const submitEvent = new window.CustomEvent('input', { bubbles: true })
        inputElement.dispatchEvent(submitEvent)

        window.requestAnimationFrame(() => {
          this.submitElement.click()
        })
      }
    )
  }
}
