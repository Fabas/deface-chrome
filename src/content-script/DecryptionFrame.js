
/**
 * This class manages the container element for Deface posts. It parses
 * content from Facebook posts, and renders an iframe that contains the
 * necessary parameters for the background side to decrypt messages.
 *
 * Decryption-sandbox.html points to src/decryption-sandbox/index.html.
 */
export default class DecryptionFrame {
  /**
   * Create a DecryptionFrame.
   */
  constructor (element, content) {
    this.init = this.init.bind(this)
    this.renderFrame = this.renderFrame.bind(this)
  }

  /**
   * Parse Deface id and ciphertext from a post and initialize the decryption iframe.
   *
   * @param {Object} element - DOM element containing the encrypted post
   * @param {string} content - content of the encrypted post
   * @return {DecryptionFrame} this
   */
  init (element, content) {
    this.element = element
    this.content = content

    const regexp = /\"?([a-zA-Z0-9]{30})\"?<br>\s*\"?\s*([a-z0-9]+)\"?/
    const match = this.content.match(regexp)
    this.id = match[1]
    this.cipherText = match[2]

    this.renderFrame()

    return this
  }

  /**
   * Render a container element sized as the initial cipherText post,
   * and insert an iframe that points to src/decryption-sandbox/index.html.
   * A Deface id and ciphertext are passed as parameters to the iframe.
   */
  renderFrame () {
    const boundingBox = this.element.getBoundingClientRect()

    this.frame = document.createElement('div')
    this.frame.setAttribute('id', `frame-${this.id}`)
    this.frame.className = 'decryption-frame'
    this.frame.style.width = `${boundingBox.width}px`
    this.frame.style.height = `${boundingBox.height}px`

    this.element.innerHTML = ''
    this.element.appendChild(this.frame)

    this.dialog = document.createElement('iframe')
    this.dialog.setAttribute('id', `dialog-${this.id}`)
    this.dialog.className = 'decryption-frame-dialog'
    this.dialog.setAttribute('frameBorder', 0)
    this.dialog.setAttribute('scrolling', 'no')
    this.dialog.style.border = 'none'
    this.dialog.style.width = '100%'

    const url = window.chrome.runtime.getURL(`decryption-sandbox.html?id=${this.id}&ciphertext=${this.cipherText}`)
    this.dialog.setAttribute('src', url)
    this.frame.append(this.dialog)
  }
}
