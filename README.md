# deface-chrome

Deface is an app that prevents Facebook from reading people’s messages. Deface-chrome is Deface's client for Chrome in the form of a browser extension.

![Deface modules](https://deface.app/images/deface-modules.png "Deface modules")

## Install & Use

To run deface-chrome, you need to create a file called `config.js` based on `config.js.template` and contains your own reCaptcha site key. Save that file at the root of this repo. You will also need to run deface-server and deface-signalling. More info at Deface's [home repository](https://gitlab.com/deface/deface).

## Contribute

We need help, and welcome all contributions! If you're not sure where to start, reading through [the project's homepage](https://deface.app) will be helpful. If you want to contribute with code, please have a look at our [issue board](https://gitlab.com/groups/deface/-/boards). Feel free to submit issues and (even better) merge requests. 

This project defaults to [standard](https://standardjs.com/) code style. It is a clean codestyle, and its adoption is increasing significantly, making the code that we write familiar to the majority of the developers.

## Code of Conduct

This project follows a [Code of Conduct](https://gitlab.com/deface/deface/blob/master/CODE-OF-CONDUCT.md) based on [NPM's](https://www.npmjs.com/policies/conduct).

## License

MIT